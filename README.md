# App-review setup with Jekyll



> Visual review is working only for public projects: support for private is [planned](https://gitlab.com/gitlab-org/gitlab/-/issues/42750). 

> Review app will work on merge requests.

1. Ask a sysadmin to enable the review-app runner for your project.
 
2. Copy the `.gitlab-ci.yml` parts to your CI configuration.

3. Add the `head.html` in your jekyll `_includes` or create it. The relevant part is:

```
{%- if jekyll.environment == "review" -%}
      <script defer
        data-project-id='{{site.project_id}}'
        data-project-path='{{site.project_namespace}}'
        data-merge-request-id='{{site.mr_id}}'
        data-mr-url='{{site.gitlab_host}}'
        id='review-app-toolbar-script'
        src='{{site.gitlab_host}}/assets/webpack/visual_review_toolbar.js'></script> 
  {%- endif -%}
``` 